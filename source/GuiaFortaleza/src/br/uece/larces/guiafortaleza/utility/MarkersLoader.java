package br.uece.larces.guiafortaleza.utility;

import java.util.List;

import android.content.Context;
import br.uece.larces.guiafortaleza.dao.InterestPointDAO;
import br.uece.larces.guiafortaleza.model.InterestPoint;

public class MarkersLoader {
	
	private List<InterestPoint> interestPoints;
	private Context context;
	
	public MarkersLoader(Context context) {
		this.context = context;
	}
	
	private void loadInterestPoints() {
		InterestPointDAO dao = new InterestPointDAO(this.context);
		
		dao.open();
		interestPoints = dao.loadAllActive();
		dao.close();
	}
	
	public List<InterestPoint> getInterestPoints() {
		loadInterestPoints();
		return interestPoints;
	}
	
}