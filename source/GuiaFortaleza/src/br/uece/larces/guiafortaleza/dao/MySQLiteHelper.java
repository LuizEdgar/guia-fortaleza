package br.uece.larces.guiafortaleza.dao;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MySQLiteHelper extends SQLiteOpenHelper {

	private Context context;

	private static final String DATABASE_NAME = "guia";
	public static final int DATABASE_VERSION = 13;

	public static final String TABLE_CATEGORY = "category";
	public static final String COLUMN_CATEGORY_ID = "category_id";
	public static final String COLUMN_CATEGORY_NAME = "category_name";
	public static final String COLUMN_CATEGORY_DESCRIPTION = "category_description";
	public static final String COLUMN_CATEGORY_ISACTIVE = "category_is_active";
	public static final String COLUMN_CATEGORY_ICON = "category_icon";

	public static final String TABLE_INTEREST_POINT = "interest_point";
	public static final String COLUMN_INTEREST_POINT_ID = "interest_id";
	public static final String COLUMN_INTEREST_POINT_NAME = "interest_name";
	public static final String COLUMN_INTEREST_POINT_DESCRIPTION = "interest_description";
	public static final String COLUMN_INTEREST_POINT_ICON = "interest_icon";
	public static final String COLUMN_INTEREST_POINT_LATITUDE = "interest_latitude";
	public static final String COLUMN_INTEREST_POINT_LONGITUDE = "interest_longitude";
	public static final String COLUMN_INTEREST_POINT_ALTITUDE = "interest_altitude";
	public static final String COLUMN_INTEREST_POINT_CATEGORY_ID = "interest_category_id";

	private static final String CREATE_TABLE_CATEGORY = "CREATE TABLE "
			+ TABLE_CATEGORY + "(" + COLUMN_CATEGORY_ID
			+ " INTEGER PRIMARY KEY, " + COLUMN_CATEGORY_NAME
			+ " TEXT NOT NULL, " + COLUMN_CATEGORY_DESCRIPTION
			+ " TEXT NOT NULL, " + COLUMN_CATEGORY_ISACTIVE
			+ " INTEGER NOT NULL, " + COLUMN_CATEGORY_ICON + " INTEGER" + "); ";

	private static final String CREATE_TABLE_INTEREST_POINT = "CREATE TABLE "
			+ TABLE_INTEREST_POINT + "(" + COLUMN_INTEREST_POINT_ID
			+ " INTEGER PRIMARY KEY, " + COLUMN_INTEREST_POINT_NAME
			+ " TEXT NOT NULL," + COLUMN_INTEREST_POINT_DESCRIPTION
			+ " TEXT NOT NULL," + COLUMN_INTEREST_POINT_ICON + " INTEGER,"
			+ COLUMN_INTEREST_POINT_LATITUDE + " REAL,"
			+ COLUMN_INTEREST_POINT_LONGITUDE + " REAL,"
			+ COLUMN_INTEREST_POINT_ALTITUDE + " REAL,"
			+ COLUMN_INTEREST_POINT_CATEGORY_ID + " INTEGER ," + "FOREIGN KEY("
			+ COLUMN_INTEREST_POINT_CATEGORY_ID + ") REFERENCES "
			+ TABLE_CATEGORY + "(" + COLUMN_CATEGORY_ID + ")" + ");";

	public MySQLiteHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		this.context = context;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_TABLE_CATEGORY);
		db.execSQL(CREATE_TABLE_INTEREST_POINT);

		try {
			InputStream inputStream = context.getAssets().open("seeds.sql");

			BufferedReader bufferedReader = new BufferedReader(
					new InputStreamReader(inputStream));

			String currentLine = "";

			while ((currentLine = bufferedReader.readLine()) != null) {
				db.execSQL(currentLine);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(MySQLiteHelper.class.getName(),
				"Upgrading database from version " + oldVersion + " to "
						+ newVersion + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORY);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_INTEREST_POINT);
		onCreate(db);
	}

}
