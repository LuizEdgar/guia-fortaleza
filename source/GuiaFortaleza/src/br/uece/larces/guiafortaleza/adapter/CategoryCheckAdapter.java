package br.uece.larces.guiafortaleza.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import br.uece.larces.guiafortaleza.R;
import br.uece.larces.guiafortaleza.model.Category;

public class CategoryCheckAdapter extends BaseAdapter {
	
	private LayoutInflater inflater;
	private List<Category> categories;
	private CategoryCheckHolder holder;
	
	public CategoryCheckAdapter(Context context, List<Category> categories) {
		this.categories = categories;
		inflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		return categories.size();
	}

	@Override
	public Category getItem(int position) {
		return categories.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		Category category = categories.get(position);
		
		if(view == null){
			view = inflater.inflate(br.uece.larces.guiafortaleza.R.layout.single_category, null);
			holder = new CategoryCheckHolder();
			holder.nomeCategory = (CheckedTextView) view.findViewById(R.id.category_name);
		}
		
		holder.nomeCategory.setText(category.getName());
		holder.nomeCategory.setChecked(category.isActive());
	
		return view;
	}
	

}

class CategoryCheckHolder{
	public CheckedTextView nomeCategory;

}
