package br.uece.larces.guiafortaleza;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;

public class TelaSplash extends Activity{
	
	private final int TEMPO_DELAY = 1100; 
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {	
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		
		ImageView loading = (ImageView) findViewById(R.id.loading);
		loading.setBackgroundResource(R.anim.loading_animation);
		
		AnimationDrawable loadingAnimation = (AnimationDrawable) loading.getBackground();
		loadingAnimation.start();
				
		new Handler().postDelayed(new Runnable() {
			
			@Override
			public void run() {
			
					Intent i = new Intent(TelaSplash.this, MainActivity.class);
					startActivity(i);
					finish();
					overridePendingTransition(R.anim.mainfadein, R.anim.splashfadeout);
				
				
				
			}
		},TEMPO_DELAY);
		
		
	}

}
