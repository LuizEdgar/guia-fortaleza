package br.uece.larces.guiafortaleza.utility;

import android.location.Location;

public class GlobalData {

	private static Location myLocation;
	
	public static void setLocation(Location location) {
		GlobalData.myLocation = location;
	}
	
	public static Location getMyLocation() {
		return myLocation;
	}
	
}
