package br.uece.larces.guiafortaleza;

import java.util.HashMap;
import java.util.List;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.Menu;
import android.widget.Toast;
import br.uece.larces.guiafortaleza.model.InterestPoint;
import br.uece.larces.guiafortaleza.utility.FragmentLocationActivity;
import br.uece.larces.guiafortaleza.utility.MarkersLoader;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapActivity extends FragmentLocationActivity {

	private GoogleMap mMap;
	private UiSettings mUiSettings;
	private MarkersLoader markersLoader;
	private List<InterestPoint> interestPoints;

	private HashMap<Marker, Integer> pointHashMap;

	private OnMarkerClickListener clickListener = new OnMarkerClickListener() {

		@Override
		public boolean onMarkerClick(Marker marker) {
			Intent intent = new Intent(getApplicationContext(),
					InterestPointDetails.class);
			int interestPointIdClicked = pointHashMap.get(marker);
			intent.putExtra("ip_id", interestPointIdClicked);
			Toast.makeText(getApplicationContext(),
					"Marker clicado, ip_id: " + interestPointIdClicked,
					Toast.LENGTH_LONG).show();
			startActivity(intent);
			return false;
		}
	};

	private void drawMarkers() {
		mMap.clear();
		for (InterestPoint interestPoint : interestPoints) {
			LatLng latLng = new LatLng(interestPoint.getLatitude(),
					interestPoint.getLongitude());
			Marker m = mMap.addMarker(new MarkerOptions().position(latLng)
					.snippet(interestPoint.getId()+""));
			pointHashMap.put(m, interestPoint.getId());
		}
	}

	private void setUpMarkers() {
		markersLoader = new MarkersLoader(this);
		interestPoints = markersLoader.getInterestPoints();
		pointHashMap = new HashMap<Marker, Integer>();
		drawMarkers();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (isGooglePlay()) {
			setContentView(R.layout.activity_map);

			setUpMapIfNeeded();
			mMap.setMyLocationEnabled(true);
			mUiSettings.setRotateGesturesEnabled(false);
		}
		setUpMarkers();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.global_menu, menu);
		return true;
	}

	private boolean isGooglePlay() {
		int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

		if (status == ConnectionResult.SUCCESS) {
			return (true);
		} else {
			Toast.makeText(this, "Google Play n�o est� dispon�vel",
					Toast.LENGTH_LONG).show();
		}
		return false;
	}

	private void setUpMap() {
		mMap.setMyLocationEnabled(true);
		mMap.setOnMarkerClickListener(clickListener);
		mUiSettings = mMap.getUiSettings();
	}

	private void setUpMapIfNeeded() {
		if (mMap == null) {

			mMap = ((SupportMapFragment) getSupportFragmentManager()
					.findFragmentById(R.id.map)).getMap();

			if (mMap != null) {
				setUpMap();
			}
		}
	}
	
	@Override
	public void onLocationChanged(Location location) {
		super.onLocationChanged(location);
//		double lat = location.getLatitude();
//		double lng = location.getLongitude();
//		LatLng coordinate = new LatLng(lat, lng);
//		Toast.makeText(this, "Nova localiza��o " + lat + "," + lng,
//				Toast.LENGTH_LONG).show();
//		mMap.animateCamera(CameraUpdateFactory.newLatLng(coordinate));
//		setUpMarkers();

	}

}
