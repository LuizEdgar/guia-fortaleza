package br.uece.larces.guiafortaleza;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckedTextView;
import android.widget.ListView;
import br.uece.larces.guiafortaleza.adapter.CategoryCheckAdapter;
import br.uece.larces.guiafortaleza.augmentedreality.AugmentedRealityActivity;
import br.uece.larces.guiafortaleza.dao.CategoryDAO;
import br.uece.larces.guiafortaleza.model.Category;

public class CategoriesActivity extends Activity {

	private CategoryCheckAdapter checkAdapter;
	private ListView categoriesListView;

	private ListView getCategoriesListView() {
		if (categoriesListView == null) {
			categoriesListView = (ListView) findViewById(R.id.categories_list);
		}
		return categoriesListView;
	}
	
	public void openMap(View v) {
		startActivity(new Intent(this, MapActivity.class));

	}
	
	public void openAR(View v){
		startActivity(new Intent(this, AugmentedRealityActivity.class));
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_categories);

		CategoryDAO cadao = new CategoryDAO(this);

		cadao.open();
		List<Category> categories = cadao.loadAll();
		cadao.close();

		checkAdapter = new CategoryCheckAdapter(getApplicationContext(),
				categories);
		getCategoriesListView().setAdapter(checkAdapter);

		getCategoriesListView().setOnItemClickListener(
				new OnItemClickListener() {
					public void onItemClick(AdapterView<?> adapter, View v,
							int position, long id) {
						CheckedTextView cv = (CheckedTextView) v;
						cv.toggle();
						Category cat = checkAdapter.getItem(position);
						cat.setActive(cv.isChecked());
						CategoryDAO dao = new CategoryDAO(getApplicationContext());
						dao.open();
						dao.update(cat);
						dao.close();
					}
				});

	}
}
