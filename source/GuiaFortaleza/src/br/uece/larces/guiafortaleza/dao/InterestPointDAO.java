package br.uece.larces.guiafortaleza.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import br.uece.larces.guiafortaleza.model.InterestPoint;

public class InterestPointDAO extends GenericDAO<InterestPoint> {

	private static final String TABLE_NAME = MySQLiteHelper.TABLE_INTEREST_POINT;
	private static final String PRIMARY_KEY = MySQLiteHelper.COLUMN_INTEREST_POINT_ID;

	public InterestPointDAO(Context context) {
		super(context);
	}

	@Override
	public long insert(InterestPoint obj) {
		return database.insert(TABLE_NAME, null, createContentValues(obj));
	}

	@Override
	public long update(InterestPoint obj) {
		return database.update(TABLE_NAME, createContentValues(obj),
				PRIMARY_KEY + "= ?", new String[] { obj.getId() + "" });
	}

	@Override
	public long delete(InterestPoint obj) {
		return database.delete(TABLE_NAME, PRIMARY_KEY + "= ?",
				new String[] { obj.getId() + "" });
	}

	@Override
	public InterestPoint load(Integer id) {
		InterestPoint point = null;

		Cursor cursor = database.query(TABLE_NAME, null, PRIMARY_KEY + "= ?",
				new String[] { id + "" }, null, null, PRIMARY_KEY);
		
		Log.i("BDGuia", "Cursor carregado.");

		
		if (cursor.getCount() > 0) {
			point = new InterestPoint();
			
			cursor.moveToFirst();

			point.setId(cursor.getInt(0));
			point.setName(cursor.getString(1));
			point.setDescription(cursor.getString(2));
			point.setIcon(cursor.getInt(3));
			point.setLatitude(cursor.getDouble(4));
			point.setLongitude(cursor.getDouble(5));
			point.setAltitude(cursor.getDouble(6));
			point.setCategoryId(cursor.getInt(7));
			
			Log.i("BDGuia", "Ponto criado.");
		}
		return point;
	}

	@Override
	public List<InterestPoint> loadAll() {
		List<InterestPoint> points = null;

		Cursor cursor = database.query(TABLE_NAME, null, null, null, null,
				null, PRIMARY_KEY + " ASC");

		if (cursor.getCount() > 0) {
			points = new ArrayList<InterestPoint>();

			while (cursor.moveToNext()) {

				InterestPoint point = new InterestPoint();

				point.setId(cursor.getInt(0));
				point.setName(cursor.getString(1));
				point.setDescription(cursor.getString(2));
				point.setIcon(cursor.getInt(3));
				point.setLatitude(cursor.getDouble(4));
				point.setLongitude(cursor.getDouble(5));
				point.setAltitude(cursor.getDouble(6));
				point.setCategoryId(cursor.getInt(7));

				points.add(point);
			}
		}

		return points;
	}
	
	public List<InterestPoint> loadAllActive() {
		List<InterestPoint> points = new ArrayList<InterestPoint>();
		
		String MY_QUERY = "SELECT * FROM "+ TABLE_NAME +" a INNER JOIN "+ MySQLiteHelper.TABLE_CATEGORY +" b ON a."+MySQLiteHelper.COLUMN_INTEREST_POINT_CATEGORY_ID+"=b."+MySQLiteHelper.COLUMN_CATEGORY_ID+" WHERE b."+MySQLiteHelper.COLUMN_CATEGORY_ISACTIVE+"=1";

		Cursor cursor = database.rawQuery(MY_QUERY, null);

		if (cursor.getCount() > 0) {

			while (cursor.moveToNext()) {

				InterestPoint point = new InterestPoint();

				point.setId(cursor.getInt(0));
				point.setName(cursor.getString(1));
				point.setDescription(cursor.getString(2));
				point.setIcon(cursor.getInt(3));
				point.setLatitude(cursor.getDouble(4));
				point.setLongitude(cursor.getDouble(5));
				point.setAltitude(cursor.getDouble(6));
				point.setCategoryId(cursor.getInt(7));

				points.add(point);
			}
		}

		return points;
	}


	@Override
	protected ContentValues createContentValues(InterestPoint obj) {
		ContentValues values = new ContentValues();
		values.put(MySQLiteHelper.COLUMN_INTEREST_POINT_ID, obj.getId());
		values.put(MySQLiteHelper.COLUMN_INTEREST_POINT_NAME, obj.getName());
		values.put(MySQLiteHelper.COLUMN_INTEREST_POINT_DESCRIPTION, obj.getDescription());
		values.put(MySQLiteHelper.COLUMN_INTEREST_POINT_ICON, obj.getIcon());
		values.put(MySQLiteHelper.COLUMN_INTEREST_POINT_LATITUDE, obj.getLatitude());
		values.put(MySQLiteHelper.COLUMN_INTEREST_POINT_LONGITUDE, obj.getLongitude());
		values.put(MySQLiteHelper.COLUMN_INTEREST_POINT_ALTITUDE, obj.getAltitude());
		values.put(MySQLiteHelper.COLUMN_INTEREST_POINT_CATEGORY_ID, obj.getCategoryId());
		return values;
	}

}
