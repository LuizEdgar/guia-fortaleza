//package br.uece.larces.guiafortaleza.augmentedreality;
//
//import java.io.ByteArrayOutputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.util.ArrayList;
//import java.util.List;
//
//import org.json.JSONArray;
//import org.json.JSONObject;
//
//import android.content.Context;
//import android.content.res.Resources;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.graphics.Color;
//import android.util.Log;
//import br.uece.larces.guiafortaleza.R;
//
//public class FortalezaDataSource extends DataSource {
//
//	private List<Marker> cachedMarkers = new ArrayList<Marker>();
//	private static Bitmap icon = null;
//	private Resources res;
//	private Context context;
//
//	public FortalezaDataSource(Resources res, Context context) {
//		if (res == null)
//			throw new NullPointerException();
//		this.context = context;
//		this.res = res;
//		createIcon();
//	}
//
//	protected void createIcon() {
//		if (res == null)
//			throw new NullPointerException();
//
//		icon = BitmapFactory.decodeResource(res, R.drawable.ic_launcher);
//	}
//
//	public List<Marker> getMarkers() {
////		Marker atl = new IconMarker("Sul",-3.767941,-38.54341, 0,
////				Color.DKGRAY, icon);
////		cachedMarkers.add(atl);
////		
////		Marker atl2 = new IconMarker("Norte",-3.760137,-38.540985, 0,
////				Color.DKGRAY, icon);
////		cachedMarkers.add(atl2);
////		
////		Marker atl3 = new IconMarker("Leste",-3.765211,-38.53753, 0,
////				Color.DKGRAY, icon);
////		cachedMarkers.add(atl3);
////		
////		Marker atl4 = new IconMarker("Oeste",-3.762878,-38.545888, 0,
////				Color.DKGRAY, icon);
////		cachedMarkers.add(atl4);
////		
////		Marker atl5 = new IconMarker("Ch�cara Para�so", -3.766057,-38.542723, 0,
////				Color.DKGRAY, icon);
////		cachedMarkers.add(atl5);
////		
////		
////
////		Marker home = new Marker("Mt Laurel", -3.775360, -38.531998, 0, Color.YELLOW);
////		cachedMarkers.add(home);
//		
////		Marker home = new Marker("Semin�rio da Prainha", -3.775360, -38.531998, 0, Color.YELLOW);
////		cachedMarkers.add(home);
////		
//
//
//		cachedMarkers.addAll(parseString());
//		
//		return cachedMarkers;
//	}
//
//	private List<Marker> parseString() {
//		List<Marker> jsonMarkers = new ArrayList<Marker>();
//		InputStream stream = null;
//		stream = this.context.getResources().openRawResource(R.raw.fortaleza);
//		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//
//		int ctr;
//		try {
//			ctr = stream.read();
//			while (ctr != -1) {
//				byteArrayOutputStream.write(ctr);
//				ctr = stream.read();
//			}
//			stream.close();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		Log.v("Text Data", byteArrayOutputStream.toString());
//		try {
//			JSONObject jo = null;
//			// Parse the data into jsonobject to get original data in form of
//			// json.
//			JSONObject jObject = new JSONObject(
//					byteArrayOutputStream.toString());
//			JSONObject jObjectResult = jObject.getJSONObject("kml");
//			jObjectResult = jObjectResult.getJSONObject("Document");
//			JSONArray jArray = jObjectResult.getJSONArray("Placemark");			
//
//			int top = Math.min(1000, jArray.length());
//			for (int i = 0; i < top; i++) {
//				jo = jArray.getJSONObject(i);
//
//				Marker ma = processJSONObject(jo);
//				if (ma != null)
//					jsonMarkers.add(ma);
//			}
//
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//		return jsonMarkers;
//	}
//
//	private Marker processJSONObject(JSONObject jo) {
//
//		if (jo == null)
//			throw new NullPointerException();
//
//		Marker ma = null;
//
//		try {
//			Double lat = null, lon = null;
//
//			JSONObject geo = jo.getJSONObject("Point");
//		
//		    String[] b = geo.getString("coordinates").split(",");  
//		  
//			lat = Double.parseDouble(b[1]);
//			lon = Double.parseDouble(b[0]);
//			
//			if (res == null)
//				throw new NullPointerException();
//			
//			
//			if (lat != null) {
//				ma = new IconMarker(jo.getString("name"), lat,
//						lon, 0, Color.RED, icon);
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return ma;
//	}
//
//}
