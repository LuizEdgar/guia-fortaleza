package br.uece.larces.guiafortaleza.dao;

import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public abstract class GenericDAO<T> {

	protected MySQLiteHelper sqLiteHelper;
	protected SQLiteDatabase database;
	
	public  GenericDAO(Context context){
		sqLiteHelper = new MySQLiteHelper(context);
	}
	
	public void open() throws SQLException{
		database = sqLiteHelper.getWritableDatabase();
	}
	
	public void close(){
		sqLiteHelper.close();
	}
	
	public abstract long insert(T obj);
	public abstract long update(T obj);
	public abstract long delete(T obj);
	public abstract T load(Integer id);
	
	public abstract List<T> loadAll();
	
	protected abstract ContentValues createContentValues(T obj);
	
}
