package br.uece.larces.guiafortaleza.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import br.uece.larces.guiafortaleza.model.Category;

public class CategoryDAO extends GenericDAO<Category> {

	private static final String TABLE_NAME = MySQLiteHelper.TABLE_CATEGORY;
	private static final String PRIMARY_KEY = MySQLiteHelper.COLUMN_CATEGORY_ID;

	public CategoryDAO(Context context) {
		super(context);
	}
	

	@Override
	public long insert(Category obj) {
		return database.insert(TABLE_NAME, null, createContentValues(obj));		
		
	}

	@Override
	public long update(Category obj) {
		return database.update(TABLE_NAME, createContentValues(obj),
				PRIMARY_KEY + "= ?", new String[] { obj.getId() + "" });
	}

	@Override
	public long delete(Category obj) {
		return database.delete(TABLE_NAME, PRIMARY_KEY + "= ?", new String[] { obj.getId() + "" });
	}

	@Override
	public Category load(Integer id) {
		Category category = null;
		Cursor cursor = database.query(TABLE_NAME, null, PRIMARY_KEY + "= ?",
				new String[] { id + "" }, null, null, null);
		if (cursor.getCount() > 0) {
			category = new Category();
			category.setId(cursor.getInt(0));
			category.setName(cursor.getString(1));
			category.setDescription(cursor.getString(2));
			category.setActive(cursor.getInt(3) != 0);
			category.setIcon(cursor.getInt(4));
		}
		return category;
	}

	@Override
	public List<Category> loadAll() {
		List<Category> categories = null;
		Cursor cursor = database.query(TABLE_NAME, null, null, null, null,
				null, PRIMARY_KEY + " ASC");

		if (cursor.getCount() > 0) {
			categories = new ArrayList<Category>();

			while (cursor.moveToNext()) {
				Category category = new Category();
				
				category.setId(cursor.getInt(0));
				category.setName(cursor.getString(1));
				category.setDescription(cursor.getString(2));
				category.setActive(cursor.getInt(3) != 0);
				category.setIcon(cursor.getInt(4));

				categories.add(category);
			}
		}

		return categories;
	}
	
	public List<Category> loadAllActive() {
		List<Category> categories =  new ArrayList<Category>();

		Cursor cursor = database.query(TABLE_NAME, null, MySQLiteHelper.COLUMN_CATEGORY_ISACTIVE + "= ?",
				new String[] { 1 + "" }, null,
				null, PRIMARY_KEY + " ASC");

		if (cursor.getCount() > 0) {

			while (cursor.moveToNext()) {
				Category category = new Category();
				
				category.setId(cursor.getInt(0));
				category.setName(cursor.getString(1));
				category.setDescription(cursor.getString(2));
				category.setActive(cursor.getInt(3) != 0);
				category.setIcon(cursor.getInt(4));

				categories.add(category);
			}
		}

		return categories;
	}
	

	@Override
	protected ContentValues createContentValues(Category obj) {
		ContentValues values = new ContentValues();
		values.put(MySQLiteHelper.COLUMN_CATEGORY_ID, obj.getId());
		values.put(MySQLiteHelper.COLUMN_CATEGORY_DESCRIPTION, obj.getDescription());
		values.put(MySQLiteHelper.COLUMN_CATEGORY_NAME, obj.getName());
		values.put(MySQLiteHelper.COLUMN_CATEGORY_ICON, obj.getIcon());
		values.put(MySQLiteHelper.COLUMN_CATEGORY_ISACTIVE, obj.isActive()?1:0);
		return values;
	}

}
