package br.uece.larces.guiafortaleza;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.widget.TextView;
import br.uece.larces.guiafortaleza.dao.InterestPointDAO;
import br.uece.larces.guiafortaleza.model.InterestPoint;

public class InterestPointDetails extends Activity {

	private TextView piName;
	private TextView piDescription;

	private int piID;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_interest_point_details);

		Bundle bundle = getIntent().getExtras();

		if (bundle != null) {
			piID = bundle.getInt("ip_id");
			Log.i("piID", "piID: " + piID);
		}

		InterestPointDAO dao = new InterestPointDAO(this);
		dao.open();
		InterestPoint point = dao.load(piID);
		dao.close();

		piName = (TextView) findViewById(R.id.ip_name);
		piDescription = (TextView) findViewById(R.id.ip_description);

		if (point != null) {
			piName.setText(point.getName());
			piDescription.setText(point.getDescription());

		} else {
			Log.e("NullPoint", "Falha ao pegar o piID: " + piID);

		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.interest_point_details, menu);
		return true;
	}

}
