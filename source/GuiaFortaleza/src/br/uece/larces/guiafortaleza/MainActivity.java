package br.uece.larces.guiafortaleza;

import br.uece.larces.guiafortaleza.augmentedreality.AugmentedRealityActivity;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {

	Button btnCategories;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		btnCategories = (Button) findViewById(R.id.btnCategories);

	}

	public void openCategories(View v) {
		startActivity(new Intent(this, CategoriesActivity.class));

		Log.i("HELPER", "metodo on click!!!");
	}

	public void openMap(View v) {
		startActivity(new Intent(this, MapActivity.class));

	}

	public void openInfo(View v) {
		startActivity(new Intent(this, ActivityInformation.class));

	}

	public void openAR(View v) {
		startActivity(new Intent(this, AugmentedRealityActivity.class));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.global_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_bar_ar_icon:
			startActivity(new Intent(this, AugmentedRealityActivity.class));
			break;

		case R.id.action_bar_map_icon:
			startActivity(new Intent(this, MapActivity.class));
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

}
