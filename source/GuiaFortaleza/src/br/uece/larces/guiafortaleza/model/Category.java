package br.uece.larces.guiafortaleza.model;

public class Category {

	private int id;
	private String name;
	private String description;
	private int icon;
	private boolean isActive; 
	
	public Category() {
		this.isActive = false;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getIcon() {
		return icon;
	}

	public void setIcon(int icon) {
		this.icon = icon;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	
	public void toggleDone(){
		this.isActive = !this.isActive;
	}
	
}
