package br.uece.larces.guiafortaleza.augmentedreality;

import java.util.List;

public abstract class DataSource {
    public abstract List<Marker> getMarkers();
}
