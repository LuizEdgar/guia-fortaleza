package br.uece.larces.guiafortaleza.utility;

import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;
import br.uece.larces.guiafortaleza.utility.GlobalData;

public abstract class FragmentLocationActivity extends FragmentActivity
		implements LocationListener {

	private LocationManager locationManager;
	private String locationProvider;
	protected Location myCurrentLocation;

	// Tempo e dist�ncia da atualiza��o da localiza��o
	private final int LOCATION_UPDATE_TIME_MILIS = 1000;//1000 * 60 * 10;
	private final int LOCATION_UPDATE_DISTANCE_METERS = 1;

	@Override
	protected void onStart() {
		super.onStart();

		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

		boolean enabledGPS = locationManager
				.isProviderEnabled(LocationManager.GPS_PROVIDER);

		if (!enabledGPS) {
			Toast.makeText(this, "GPS offline...", Toast.LENGTH_LONG).show();
			Intent intent = new Intent(
					android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			startActivity(intent);
		}

		// Criteria pega o melhor provedor de localiza��o
		Criteria criteria = new Criteria();
		locationProvider = locationManager.getBestProvider(criteria, true);
		myCurrentLocation = locationManager
				.getLastKnownLocation(locationProvider);
		GlobalData.setLocation(myCurrentLocation);

	}

	@Override
	protected void onResume() {
		super.onResume();
		Log.i("GuiaFortaleza", "onResume!! FragmentLocationActivity class.");
		locationManager.requestLocationUpdates(locationProvider,
				LOCATION_UPDATE_TIME_MILIS, LOCATION_UPDATE_DISTANCE_METERS,
				this);
	}

	@Override
	protected void onPause() {
		locationManager.removeUpdates(this);
		super.onPause();
	}

	@Override
	public void onLocationChanged(Location location) {
		Log.i("GuiaFortaleza", "onLocationChanged!! FragmentLocationActivity class.");
		myCurrentLocation = location;
		GlobalData.setLocation(location);
	}

	@Override
	public void onProviderDisabled(String provider) {
		Criteria criteria = new Criteria();
		locationProvider = locationManager.getBestProvider(criteria, true);

	}

	@Override
	public void onProviderEnabled(String provider) {
		Criteria criteria = new Criteria();
		locationProvider = locationManager.getBestProvider(criteria, true);

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

}
